//
//  TimeTrackerApp.swift
//  TimeTracker
//
//  Created by Henrique Forioni de Lima on 10/11/20.
//

import SwiftUI

@main
struct TimeTrackerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
